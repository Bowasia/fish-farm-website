import axios from 'axios';
import config from './config/config.json'

const BASE_URL = config.backend_url

const generateApiUrl = (endpoint, dynamicParams = []) => {
  let url = `${BASE_URL}${endpoint}`;
  
  if (dynamicParams.length > 0) {
    dynamicParams.forEach(param => {
      url = url.replace(/\/:[^/]+/, `/${param}`);
    });
  }

  return url;
};

const apiRequest = async (method, endpoint, data = null, dynamicParams = []) => {
  const url = generateApiUrl(endpoint, dynamicParams);

  try {
    const response = await axios({
      method,
      url,
      data,
      withCredentials: true, // Include credentials for cross-origin requests (cookies)
      headers: {
        'Content-Type': 'application/json',
      },
    });

    return response.data;
  } catch (error) {
    console.error(`Error ${method} ${url}:`, error);
    throw error;
  }
};

// Example usage:
// apiRequest('GET', api.currentUser)
//   .then(data => console.log('User data:', data))
//   .catch(error => console.error('Error fetching user data:', error));

export default {
  // Auth APIs
  login: () => apiRequest('GET', '/api/auth/login'),
  logout: () => apiRequest('GET', '/api/auth/logout'),
  currentUser: () => apiRequest('GET', '/api/user'),

  // Sensor group
  getSensorData: (data) => apiRequest('POST', '/api/getSensorData', data),
  getLatestSensorData: () => apiRequest('GET', '/api/getLatestSensorData'),

  // camera group
  getImages: (data) => apiRequest('POST', '/api/fetch-images', data),
  getImageById: (data) => apiRequest('POST', '/api/fetch-image-by-id', data),

  // food group
  getRevenue: (data) => apiRequest('POST', '/api/fetch-revenue', data),
  getCountCoin: () => apiRequest('GET', '/api/coin-allocation'),
  getTransaction: (data) => apiRequest('POST', '/api/fetch-transaction', data),
  getTotalCoin: () => apiRequest('GET', '/api/total-coin'),
  getFoodLog: (data) => apiRequest('POST', '/api/fetch-food-log', data),
  getFoodAvailiable: () => apiRequest('GET', '/api/fetch-availiable-food')
};
