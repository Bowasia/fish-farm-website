import React from "react";
import { NavLink } from "react-router-dom";
import kmitlLogo from "../component/asset/logo.png";
import "../style/NavbarStyle.css";

const NavBar = () => {
  return (
    <nav className="nav-section">
      <div className="nav-left">
        <div className="logo-container">
          <img src={kmitlLogo} alt="KMITL Logo" className="kmitllogo-image" />
          <h2 className="bold">
            RAI Smart Fish
          </h2>
        </div>
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink to="/feeder" className="nav-link" activeClassName="active">
              Feedy
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/sensorMonitoring" className="nav-link" activeClassName="active">
              Sensors
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/imageGallery/1" className="nav-link" activeClassName="active">
              Fish Camera
            </NavLink>
          </li>
        </ul>
      </div>
      <button type="button" className="login-button">LOG IN</button>
    </nav>
  );
};

export default NavBar;
