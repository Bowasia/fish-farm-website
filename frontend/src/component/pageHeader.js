import React from "react";
import "../App.css";
import { useMediaQuery } from "react-responsive";
import DateRangePickerCustom from "./DateRangePickerCustom";
function PageHeader({ title, onDateRangeChange, defaultDate  }) {
  return (
    <div className="scrollbar-hidden">
      <div className={`fixed overflow-hidden z-[2] m-0 p-5 w-full bg-custom-color `} >
        <div className="relative text-xl px-2 font-bold text-left">{title}</div>
        <DateRangePickerCustom onDateRangeChange={onDateRangeChange} defaultDate={defaultDate}/>
      </div>
    </div>
  );
}
export default PageHeader;
