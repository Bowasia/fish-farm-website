import React, { useState, useEffect } from "react";
import FDBox from "../component/FDBox";
import { LineChart } from "@mui/x-charts/LineChart";

function Chart({ data }) {
  const xData = data.map((entry) => {
    const utcPlus7Time = new Date(entry.recorded_when);
    const formattedTime = utcPlus7Time.toLocaleString([], {
      timeZone: "UTC",
    });
    return formattedTime
  });
  const keyToLabel = {
    temperature: "Temperature (°C)",
    dissolved_oxygen: "Dissolve Oxygen (ppm)",
    electric_conductivity: "Electric Conductivity (ms/cm)",
    pH: "pH (unit)",
  };

  const customize = {
    height: 400,
    legend: { hidden: false },
    margin: { top: 70 },
    stackingOrder: "descending",
  };
  return (
    <div>
      <FDBox className="bg-white">
        <LineChart
          dataset={data}
          {...customize}
          xAxis={[
            {
              dataKey: "index",
              valueFormatter: (value) => xData[value],
            },
          ]}
          series={Object.keys(keyToLabel).map((key) => ({
            dataKey: key,
            label: keyToLabel[key],
            showMark: true,
          }))}
        />
      </FDBox>
    </div>
  );
}
export default Chart;
