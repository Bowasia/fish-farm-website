import React from "react";
import { NavLink } from "react-router-dom";
import "../style/DesignSystem.css";
import "../style/WebpageStyle.css";
import AboutCard from "../component/About";
import raiLogo from "../component/asset/rai-logo.png";
import water from "../component/asset/water-webpage.jpg";
import NavBar from "../component/nav-bar";

import feedymain from "../component/asset/feedy-main.jpeg";
import feedy1 from "../component/asset/feedy-1.jpeg";
import feedy2 from "../component/asset/feedy-2.jpeg";
import feedy3 from "../component/asset/feedy-3.jpeg";

import watermain from "../component/asset/water-main.jpeg";
import water1 from "../component/asset/water-1.jpeg";
import water2 from "../component/asset/water-2.jpeg";
import water3 from "../component/asset/water-3.jpeg";

import fishcammain from "../component/asset/fishcam-main.jpeg";
import fishcam1 from "../component/asset/fishcam-1.jpeg";
import fishcam2 from "../component/asset/fishcam-2.jpeg";
import fishcam3 from "../component/asset/fishcam-3.jpeg";

const Webpage = () => {
  return (
    <div>

      <NavBar></NavBar>

      <div className="overflow-auto">
      <section className="about-section">
        <p>
        Your ultimate fish pond manager, 
        monitoring <span className="span-about-text">water quality</span>, <span className="span-about-text">automating feeding</span>, 
        and <span className="span-about-text">detecting fish activity</span> to simplify your pond care.
        </p>
      </section>

      
      <img src={water} alt="water" className="water-image" />
      

      <section className="highlight-section">
        <div className="highlight-text">
          <h2 className="bold">Highlights</h2>
          <p>Scientific research and artistic activities are carried out at six schools and their departments and units.</p>
        </div>
        <div className="highlight-container">
        <AboutCard
          mainPic={feedymain}
          projectName="Feedy"
          beforeViewContent="At the heart of our Feedy lies a steadfast vision: to redefine fish feeding practices while fostering community engagement and promoting environmental stewardship. Through seamless integration and advanced technology, we prioritize the welfare of aquatic life, inviting active participation from individuals like you. With a dedicated focus on transparency and accountability, our real-time monitoring system ensures that every contribution makes a tangible impact on the health of our oceans and waterways. "
          img1={feedy1}
          img2={feedy2}
          img3={feedy3}
          afterViewContent="Join us in embracing the convergence of technology, philanthropy, and environmental consciousness as we work together towards a more sustainable future for all."
        />

        <AboutCard
          mainPic={watermain}
          projectName="Water Quality"
          beforeViewContent="Experience the evolution of aquaculture monitoring with our IoT solution. Through real-time data insights and prompt corrective actions, the system promotes transparency and accountability among stakeholders. Despite potential challenges such as initial setup costs and technological limitations, the application of IoT technology in environmental conservation shows immense promise, not only within educational institutions but also in broader societal efforts towards a greener future."
          img1={water1}
          img2={water2}
          img3={water3}
          afterViewContent="Our project underscores the importance of ongoing research and innovation to expand the reach and effectiveness of IoT applications in addressing environmental challenges on a larger scale."
        />

        <AboutCard
          mainPic={fishcammain}
          projectName="Fish Camera"
          beforeViewContent="Dive into the depths with fish cameras, where cutting-edge technology meets the wonders of the underwater world. Equipped with on-ground and underwater cameras, these marvels capture captivating video footage. But that's just the beginning! Harnessing the power of AI, they decipher fish species and population dynamics, creating a mesmerizing tapestry of aquatic life. Seamlessly transmitting data, they unveil this hidden world on a dynamic and engaging dashboard, inviting you to explore and marvel at the secrets beneath the surface."
          img1={fishcam1}
          img2={fishcam2}
          img3={fishcam3}
          afterViewContent="Additional content Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        />
       
        </div>
      </section>

      <section className="before-end-section">
        <h1 className="bold">RAI Smart Fish</h1>
        <p>
          Introducing RAI Smart Fish, a solution designed for controlling and managing fish pond locations.
          Our service offers quality measurement capabilities, assessing the water quality and waste levels
          in the area to ensure optimal conditions for fish health. Additionally, we provide automated fish feeding,
          allowing users to feed their fish remotely. Furthermore, our fish detection feature enables the identification
          and monitoring of fish within the pond.
        </p>
      </section>

      <section className="location-section">
        <img src={raiLogo} alt="RAI Logo" className="railogo-image" />
        <p className="semibold">King Mongkut's Institute of Technology Ladkrabang</p>
        <p>HM Building, 3 Thanon Chalong Krung, Lat Krabang, Bangkok 10520</p>
        <p className="semibold">© 2024 Robotics and Artificial Intelligence Engineering, KMITL | All Rights Reserved</p>
      </section>

      </div>
    </div>
  );
};

export default Webpage;
