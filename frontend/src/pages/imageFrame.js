import React, { useState, useEffect } from "react";
import API from "../api";
import "../style/loading.css";
import ReqTimedOut from "../component/http error/408";
import PageHeader from "../component/pageHeader";
import MainContent from "../component/mainContent";
import { Buffer } from "buffer";
import { useParams, Link } from "react-router-dom";
import { IoIosArrowBack } from "react-icons/io";

function ImageFrame() {
  const { page, id } = useParams();
  const [images, setImages] = useState();
  const [loading, setLoading] = useState(true);
  const [reqTimedOut, setReqTimedOut] = useState(false);

  useEffect(() => {
    const fetchImages = async () => {
      try {
        const payload = { image_id: id };
        const response = await API.getImageById(payload);
        setImages(response);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching image by id:", error);
        setLoading(false);
      }
    };

    // Fetch images initially
    fetchImages();
  }, []);

  if (loading) {
    return (
      <div className="loading-spinner">
        <div className="spinner"></div>
      </div>
    );
  }
  if (reqTimedOut) {
    return <ReqTimedOut />;
  }

  return (
    <div>
      <PageHeader title="Image Preview" />
      <MainContent>
        <div className="">
          <Link
            to={`/imageGallery/${page}`} // Use a route for individual images
            className="m-2 cursor-pointer flex py-1"
          >
            <IoIosArrowBack size={25} />
            Back to Gallery
          </Link>
          <img
            src={`data:image/png;base64,${Buffer.from(
              images.data.data
            ).toString("base64")}`}
            alt={images.filename}
            className="w-full h-full object-cover"
          />
          <div>
            {images.metadata.map((metadata, index) => (
              <div key={index}>
                {metadata.breed_name} : {metadata.fish_amount}
              </div>
            ))}
          </div>
        </div>
      </MainContent>
    </div>
  );
}

export default ImageFrame;
