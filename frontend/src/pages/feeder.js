import React, { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import API from "../api";
import { DataGrid } from "@mui/x-data-grid";
import { BarChart } from "@mui/x-charts/BarChart";
import FDBox from "../component/FDBox";
import PageHeader from "../component/pageHeader";
import MainContent from "../component/mainContent";
import { motion } from "framer-motion"
import { LineChart } from '@mui/x-charts';
import { PieChart } from '@mui/x-charts/PieChart';
import { GridColDef } from "@mui/x-data-grid";

const { add,sub } = require("date-fns");

const columns = [
  {
    field: "transaction_id",
    headerName: "ID",
    // headerAlign: "center",
    width: 50,
  },
  {
    field: "feeding_log_id",
    headerName: "Feed ID",
    // headerAlign: "center",
    width: 80,
  },
  {
    field: "feeder_id",
    headerName: "Machine ID",
    headerAlign: "center",
    width: 100,
  },
  {
    field: "money_value",
    headerName: "Coin",
    headerAlign: "center",
    width: 50,
  },
  {
    field: "recorded_when",
    headerName: "Timestamp",
    headerAlign: "center",
    width: 200,
  },
];

const rows = [
  { id: 1, lastName: "Snow", firstName: "Jon", age: 14 },
  { id: 2, lastName: "Lannister", firstName: "Cersei", age: 31 },
  { id: 3, lastName: "Lannister", firstName: "Jaime", age: 31 },
  { id: 4, lastName: "Stark", firstName: "Arya", age: 11 },
  { id: 5, lastName: "Targaryen", firstName: "Daenerys", age: null },
  { id: 6, lastName: "Melisandre", firstName: null, age: 150 },
  { id: 7, lastName: "Clifford", firstName: "Ferrara", age: 44 },
  { id: 8, lastName: "Frances", firstName: "Rossini", age: 36 },
  { id: 9, lastName: "Roxie", firstName: "Harvey", age: 65 },
];

const colorPalette = ["#FFB534", "#E75716", "#F9B572", "#ED7D31", "#6C5F5B"];

const transaction = [
  {
    transactionID: 781923,
    totalCoin: 15,
    timestamp: "2024-03-13 00:27:27",
  },
  {
    transactionID: 314568,
    totalCoin: 30,
    timestamp: "2024-03-13 00:27:26",
  },
  {
    transactionID: 520179,
    totalCoin: 0,
    timestamp: "2024-03-13 00:27:25",
  },
  {
    transactionID: 903841,
    totalCoin: 75,
    timestamp: "2024-03-13 00:27:24",
  },
  {
    transactionID: 172496,
    totalCoin: 60,
    timestamp: "2024-03-13 00:27:23",
  },
  {
    transactionID: 837152,
    totalCoin: 15,
    timestamp: "2024-03-13 00:27:22",
  },
  {
    transactionID: 419207,
    totalCoin: 45,
    timestamp: "2024-03-13 00:27:21",
  },
  {
    transactionID: 651389,
    totalCoin: 90,
    timestamp: "2024-03-13 00:27:20",
  },
  {
    transactionID: 284710,
    totalCoin: 30,
    timestamp: "2024-03-13 00:27:19",
  },
  {
    transactionID: 105932,
    totalCoin: 75,
    timestamp: "2024-03-13 00:27:18",
  },
];

function getRowId(transaction) {
  return transaction.transactionID;
}

const date = [];
const value = [];
const recentTotalcoin = transaction[transaction.length - 1].totalCoin;

transaction.forEach((item) => {
  date.push(new Date(item.timestamp));
});

transaction.forEach((item) => {
  value.push(item.totalCoin);
});

console.log(date);
function getColumn(data, ColName) {
  const res = [];
  data.forEach((log) => {
    res.push(log.ColName);
  });
  return res;
}

// function Feeder () {
//   const [selectedDateRange, setSelectedDateRange] = useState([
//     new Date("2024-01-01"),
//     new Date("2024-12-01"),
//   ]);
//   const handleDateRangeChange = (dateRange) => {
//     setSelectedDateRange(dateRange);
//     console.log(selectedDateRange)
//   };

const Feeder = () => {
  const [Revenue, setRevenue] = useState([])
  const [Revenue_date, setRevenue_date] = useState(['2024-03-23', '2024-03-24', '2024-03-25', '2024-03-27', '2024-03-28', '2024-03-29', '2024-03-30'])
  const [Revenue_value, setRevenue_value] = useState(['15', '75', '120', '30', '111', '400', '340'])
  const [TotalCoin, setTotalCoin] = useState('')
  const [FoodAvailable, setFoodAvailable] = useState('')
  const [CountCoin, setCountCoin] = useState([])
  const [Coin_1, setCoin_1] = useState(29)
  const [Coin_5, setCoin_5] = useState(35)
  const [Coin_10, setCoin_10] = useState(21)
  const [Transaction, setTransaction] = useState([])
  const [FoodLog, setFoodLog] = useState([[],[]])
  const [selectedDateRange, setSelectedDateRange] = useState([
    new Date("2024-03-01"),
    new Date("2024-03-31"),
  ]);
  const [FoodLog_start, setFoodLog_start] = useState(selectedDateRange[0])
  const [FoodLog_end, setFoodLog_end] = useState(selectedDateRange[1])
  const [FoodLog_date, setFoodLog_date] = useState(date)
  const [FoodLog_value, setFoodLog_value] = useState(value)
  // FoodLog_start.setDate(selectedDateRange[0].getDate() - 1);
  // FoodLog_end.setDate(selectedDateRange[1].getDate() + 1);

  const now_date =  new Date().toLocaleDateString('en-US', {year: 'numeric', month: 'long', day: 'numeric'})
  const now_time = new Date().toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' });

  // const handleDateRangeChange = (dateRange) => {
  //   setSelectedDateRange(dateRange);
  //   console.log('date change')
  //   console.log(selectedDateRange)
  // };

  function getLatestLogsPerDay(logs, dateStart, dateEnd) {
    // Create an object to store the latest log for each day
    const latestLogs = {};
    const date = [];
    const value = []
  
    // Iterate through the logs array
    for (const log of logs) {
      // Extract the date from the "recorded_when" property (assuming it's a string in YYYY-MM-DD format)
      const logDate = log.recorded_when.substring(0, 10);
  
      // Convert date strings to Date objects
      const logDateObject = new Date(logDate);
  
      // Check if the log date is within the specified date range
      if (logDateObject >= dateStart && logDateObject <= dateEnd) {
        // Check if a log for this date already exists in the latestLogs object
        if (!latestLogs[logDate]) {
          // If not, add it with the current log
          latestLogs[logDate] = log;
          date.push(logDate)
        } else {
          // If it does exist, compare the timestamps and update the latestLogs object if necessary
          if (log.recorded_when > latestLogs[logDate].recorded_when) {
            latestLogs[logDate] = log;
          }
        }
      }
    }
    console.log('THIS IS IN getLatestLogsPerDay')
    console.log(date)
    console.log(date.map(day => {
      return latestLogs[day]?.food_percent
    }))
  
    // Return the object containing the latest log for each day within the date range
    return [date, date.map(day => { return latestLogs[day]?.food_percent})];
  }
  
  

  const fetchData = async (dateRange) => {
    try {
      const payload = {
        "date_start": dateRange[0],
        "date_end": dateRange[1]
      }

      const payload_food_log = {
        "date_start": sub(dateRange[0], {days: 1}),
        "date_end": add(dateRange[1], {days: 1})
      }

      const payload_transaction = {
        pagination_no: 6,
        fetch_limit: 20,
      };

      const Revenue_res = await API.getRevenue(payload)
        .then((res) => {
          if (res) {
            setRevenue(res);
            setRevenue_date(
              res?.map((item) => item.transaction_date.slice(0, 10))
            );
            setRevenue_value(res?.map((item) => item.total_money_value));
            console.log("REVENUE");
            console.log(Revenue);
            console.log("REVENUE DATE");
            console.log(Revenue_date);
            console.log("REVENUE VALUE");
            console.log(Revenue_value);
          }
        })
        .catch((error) => {
          console.log("API.getRevenue error:", error);
        });

      const TotalCoin_res = await API.getTotalCoin()
        .then((res) => {
          if (res) {
            setTotalCoin(res[0].total_coin);
            console.log("TOTAL COIN");
            console.log(res);
          }
        })
        .catch((error) => {
          console.log("API.getTotalCoin error:", error);
        })
      .then(res => {
        if (res){
          setTotalCoin(res[0].total_coin)
          // console.log('TOTAL COIN')
          // console.log(res)
        }
      })
      .catch(error => {
        console.log('API.getTotalCoin error:', error)
      });

      const FoodAvailable_res = await API.getFoodAvailiable()
      .then(res => {
        if (res){
          setFoodAvailable(res[0])
          // console.log('FOOD AVAILABLE')
          // console.log(res)
        }
      })
      .catch(error => {
        console.log('API.getFoodAvailable error:', error)
      });

      const CountCoin_res = await API.getCountCoin()
      .then(res => {
        if (res){
          setCountCoin(res)
          const CoinTypes = [1, 5, 10];
          const filteredCoinTypes = res.filter((item) => CoinTypes.includes(item.money_value));
          setCountCoin(filteredCoinTypes)
          // console.log('COUNT COIN')
          // console.log(res)
          // console.log('Filtered coin')
          // console.log(filteredCoinTypes)
          // console.log('COIN 1')
          // console.log(filteredCoinTypes.find((item) => item.money_value === 1).coin_count)
          setCoin_1(filteredCoinTypes.find((item) => item.money_value === 1).coin_count)
          setCoin_5(filteredCoinTypes.find((item) => item.money_value === 5).coin_count)
          setCoin_10(filteredCoinTypes.find((item) => item.money_value === 10).coin_count)
        }
      })
      .catch(error => {
        console.log('API.getCountCoin error:', error)
      });

      const Transaction_res = await API.getTransaction(payload_transaction)
        .then((res) => {
          if (res) {
            setTransaction(res);
            console.log("TRANSACTION");
            console.log(res);
          }
        })
        .catch((error) => {
          console.log("API.getTransaction error:", error);
        });

      const FoodLog_res = await API.getFoodLog(payload_food_log)
      .then(res => {
        if (res){
          setFoodLog(res)
          console.log('FOOD LOG')
          console.log(res)
          console.log('Get latest log ')
          const latestLogsPerDay = getLatestLogsPerDay(res, payload_food_log.date_start, payload_food_log.date_end);
          console.log(latestLogsPerDay);
          console.log('selectedDateRange')
          console.log(selectedDateRange)
          setFoodLog(latestLogsPerDay)
          console.log('Foodlog [0]')
          console.log(FoodLog[0])
          console.log('FoodLog[1]')
          console.log(FoodLog[1])
         
        }
      })
      .catch(error => {
        console.log('API.getFoodLog error:', error)
      });
      

      function parseMoneyValueAndCoinCount(data) {
        return data.map((item) => ({
          moneyValue: item.money_value,
          coinCount: item.coin_count,
        }));
      }

      // const Revenue_date = Revenue.map(item => item.transaction_date);
      // const Revenue_value = Revenue.map(item => item.total_money_value);
      console.log("selectedDateRange");
      console.log(selectedDateRange);

      console.log("Revenue_date");
      console.log(Revenue_date);

      console.log("Revenue_value");
      console.log(Revenue_value);

      console.log("Filter coin type");
      // console.log(filteredCoinTypes)

      console.log(FoodAvailable_res);
      console.log("Revenue_res");
      console.log(Revenue_res);
      console.log(CountCoin);
      // console.log()

      console.log("Transaction");
      console.log(Transaction);

      console.log("Food Log");
      console.log(FoodLog);

      console.log("This is count coin");
      console.log("This is Food available", FoodAvailable.food_percent);
      console.log("This is Food available log time", FoodAvailable.log_time);
      console.log("this is revenue");
      console.log(Revenue);
      console.log("this is total coin", TotalCoin);
      // setLoading(false);
    } catch (error) {
      console.error("Error fetching sensor data:", error);
      // setLoading(false);
      // setReqTimedOut(true);
    }
  };

  useEffect(() => {
    
    fetchData(selectedDateRange)
    console.log("DONE")
    console.log("REVENUEEEE")
    console.log(Revenue)
    return () => {
      console.log("This is useEffect cleanup");
    };
  }, [selectedDateRange]);

  return (
    <div>
      <PageHeader
        title={"Feedy dashboard"}
        onDateRangeChange={(date) => {
          setSelectedDateRange(date);
        }}
        defaultDate={selectedDateRange}
      />
      <MainContent>
        <div className="my-auto w-full">
          <div className="flex flex-row flex-wrap content-between items-stretch flex-grow">
            <FDBox className="basis-1/3 grow">
              <h1>Revenue</h1>
              <div>
                <BarChart
                  xAxis={[
                    {
                      scaleType: "band",
                      data: Revenue_date,
                    },
                  ]}
                  series={[{ data: Revenue_value }]}
                  width={450}
                  height={300}
                  colors={colorPalette}
                />
              </div>
            </FDBox>

            <FDBox className="basis-1/4 grow flex flex-row items-stretch">
              <div className="h-full">
                <h1>Total coin</h1>
                <div className="h-5/6 w-56 items-center flex justify-self-center">
                  <div className="text-8xl m-auto">
                    {TotalCoin}
                    <div className="text-sm text-center">Baht</div>
                  </div>
                </div>
                <div className='-mt-5'>
                  <div className='text-sm text-center text-gray-600'>Last fetch</div>
                  <div className='text-sm text-center text-gray-600'>
                    {now_date}
                    </div>
                    <div className='text-sm text-center text-gray-600'>
                    {now_time}
                    </div>
                </div>
              </div>
            </FDBox>

            <FDBox className="basis-1/5">
              <h1>Coin allocation</h1>
              <PieChart
                series={[
                  {
                    data: [
                      { id: 0, value: Coin_1, label: "1 Baht coin" },
                      { id: 1, value: Coin_5, label: "5 Baht coin" },
                      { id: 2, value: Coin_10, label: "10 Baht coin" },
                    ],
                    innerRadius: 25,
                    outerRadius: 100,
                    paddingAngle: 5,
                    cornerRadius: 8,
                    startAngle: 0,
                    endAngle: 360,
                    cx: 125,
                    cy: 150,
                  },
                ]}
                colors={colorPalette}
                width={400}
                height={300}
              />
            </FDBox>

            <FDBox className="basis-1/4 grow flex flex-row items-stretch">
              <div className="h-full">
                <h1>Food available</h1>
                <div className="h-5/6 w-56 items-center flex justify-self-center">
                  <div className="text-8xl m-auto">
                    {FoodAvailable.food_percent}
                    <div className=" text-base text-center">%</div>
                  </div>
                </div>
                <div className='-mt-3'>
                  <div className='text-sm text-center text-gray-600'>Last log</div>
                  <div className='text-sm text-center text-gray-600'>
                    {new Date(FoodAvailable.log_time).toLocaleDateString('en-US', {year: 'numeric', month: 'long', day: 'numeric'})
                     }
                  </div>
                  <div className='text-sm text-center text-gray-600'>
                    { new Date(FoodAvailable.log_time).toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit' })}
                  </div>

                </div>
              </div>
            </FDBox>

            <FDBox className=" w-3/4">
              <h1>Transaction</h1>
              <Box sx={{ height: 400, width: 500, textAlign: "center" }}>
                <DataGrid
                  width={975}
                  rows={Transaction}
                  columns={columns}
                  getRowId={(row) => row.transaction_id}
                  initialState={{
                    pagination: {
                      paginationModel: {
                        pageSize: 5,
                      },
                    },
                  }}
                  pageSizeOptions={[5]}
                  disableRowSelectionOnClick
                />
              </Box>
            </FDBox>

            <FDBox className="basis-1/4">
              <h1>Food amount</h1>
              <div className=' h-5/6 flex place-items-center'>

                  <LineChart
                    xAxis={[{ 
                      label: 'Date',
                      data: FoodLog[0],
                      scaleType: 'point'  
                    }]}
                    series={[
                      {
                        data: FoodLog[1],
                        color: '#ED7D31',
                        area: true
                      },
                    ]}
                    width={370}
                    height={300}
                    sx={{
                      '.MuiLineElement-root': {
                        strokeWidth: '4'
                      }

                    }}
                  />

              </div>
            </FDBox>
 
          </div>
        </div>
      </MainContent>
    </div>
  );
};
export default Feeder;
