CREATE PROCEDURE get_latest_all_sensor_data_v2()
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT 
        sd.sensor_data_id,
        sd.sensor_node_id,
        sd.dissolved_oxygen_ppm as 'dissolved_oxygen',
        sd.electric_conductivity_ms_cm as 'electric_conductivity',
        sd.ph as 'pH',
        sd.temperature_c as 'temperature',
        sd.recorded_when
    FROM sensor_data sd
    INNER JOIN (
        SELECT sensor_node_id, MAX(recorded_when) AS max_recorded_when
        FROM sensor_data
        GROUP BY sensor_node_id
    ) AS latest_records ON sd.sensor_node_id = latest_records.sensor_node_id
                        AND sd.recorded_when = latest_records.max_recorded_when;
                        
END


GRANT EXECUTE ON PROCEDURE get_latest_all_sensor_data_v2 TO 'app_rw'@'%';