CREATE TABLE feeding_log (
    feeding_log_id              INT AUTO_INCREMENT PRIMARY KEY,
    feeder_id                   TINYINT     NOT NULL,
    fed_amount                  FLOAT     NULL,
    operation_type_id           TINYINT     NOT NULL,
    food_tank_percent_before    TINYINT    NOT NULL,
    food_tank_percent_after     TINYINT     NULL,
    feed_status_id              TINYINT  NOT   NULL,
    process_started_when        DATETIME    NULL,
    process_finished_when       DATETIME    NULL,
    recorded_when               DATETIME    NOT NULL,
    updated_when                DATETIME    NULL
);

ALTER TABLE feeding_log
ADD CONSTRAINT fk_feeding_log_feeder_id FOREIGN KEY (feeder_id) REFERENCES feeder(feeder_id);

ALTER TABLE feeding_log
ADD CONSTRAINT fk_feeding_log_feed_status_id FOREIGN KEY (feed_status_id) REFERENCES feeding_status(status_id);

ALTER TABLE feeding_log
ADD CONSTRAINT fk_feeding_log_operation_type_id
    FOREIGN KEY (operation_type_id) 
    REFERENCES operation_type(operation_type_id);