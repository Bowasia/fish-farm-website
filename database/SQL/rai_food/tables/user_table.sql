CREATE TABLE user_table (
    user_id             INT AUTO_INCREMENT PRIMARY KEY,
    user_name           VARCHAR(128)    NULL,
    user_type_id        TINYINT         NOT NULL,
    user_identifier     VARCHAR(256)    NOT NULL, -- ex. line user id, google id
    recorded_when       DATETIME        NOT NULL,
    updated_when        DATETIME        NULL
);

ALTER TABLE user_table
ADD CONSTRAINT fk_user_table_user_type_id
    FOREIGN KEY (user_type_id) 
    REFERENCES user_type(user_type_id);