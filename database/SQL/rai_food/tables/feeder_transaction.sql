CREATE TABLE feeder_transaction (
    transaction_id          INT AUTO_INCREMENT PRIMARY KEY,
    feeding_log_id          INT         NOT NULL,
    feeder_id               TINYINT     NOT NULL,           
    payment_type_id       TINYINT     NOT NULL,
    user_id             	INT    NULL,
    money_value             TINYINT     NOT NULL, -- amount of money in
    recorded_when           DATETIME    NOT NULL
);

ALTER TABLE feeder_transaction
ADD CONSTRAINT fk_feeder_transaction_feeding_log_id
    FOREIGN KEY (feeding_log_id) 
    REFERENCES feeding_log(feeding_log_id);

ALTER TABLE feeder_transaction
ADD CONSTRAINT fk_feeder_transaction_payment_type_id
    FOREIGN KEY (payment_type_id) 
    REFERENCES payment_type(payment_type_id);

ALTER TABLE feeder_transaction
ADD CONSTRAINT fk_feeder_transaction_feeder_id
    FOREIGN KEY (feeder_id) 
    REFERENCES feeder(feeder_id);

ALTER TABLE feeder_transaction
ADD CONSTRAINT fk_feeder_transaction_user_id
    FOREIGN KEY (user_id) 
    REFERENCES user_table(user_id);