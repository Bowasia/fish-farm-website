CREATE TABLE feeder (
    feeder_id           TINYINT AUTO_INCREMENT PRIMARY KEY,
    description_text    VARCHAR(512)    NULL,
    is_active           BOOLEAN         NOT NULL,
    recorded_when       DATETIME        NOT NULL,
    updated_when        DATETIME        NULL
);