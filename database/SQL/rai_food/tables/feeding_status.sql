CREATE TABLE feeding_status (
    status_id           TINYINT AUTO_INCREMENT PRIMARY KEY,
    status_name         VARCHAR(52)    NOT NULL,
    recorded_when       DATETIME        NOT NULL,
    updated_when        DATETIME        NULL
);
