
CREATE PROCEDURE upsert_feeder_v1 (
    IN p_feeder_id TINYINT,
    IN p_description_text VARCHAR(512),
    IN p_is_active BOOLEAN
)
BEGIN
    DECLARE existing_id TINYINT;
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    
    -- Check if feeder_id already exists
    SELECT feeder_id INTO existing_id
    FROM feeder
    WHERE feeder_id = p_feeder_id;
    
    -- If feeder_id exists, update the record
    IF existing_id IS NOT NULL THEN
        UPDATE feeder
        SET description_text = p_description_text,
            is_active = p_is_active,
            updated_when = CURRENT_TIMESTAMP()
        WHERE feeder_id = p_feeder_id;
        
    -- If feeder_id does not exist, insert a new record
    ELSE
        INSERT INTO feeder (description_text, is_active, recorded_when)
        VALUES (p_description_text, p_is_active, CURRENT_TIMESTAMP());
        
    END IF;
    
END


GRANT EXECUTE ON PROCEDURE upsert_feeder_v1 TO 'app_rw'@'%';