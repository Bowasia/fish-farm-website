CREATE PROCEDURE update_feeding_log_v8(
    IN p_log_id INT,
    IN p_feed_status_name VARCHAR(52),
    IN p_fed_amount FLOAT,
    IN p_food_tank_percent_before TINYINT,
    IN p_food_tank_percent_after TINYINT,
    IN p_process_started_when DATETIME,
    IN p_process_finished_when DATETIME
)
BEGIN
    DECLARE feed_status_id_val TINYINT;
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Get feed_status_id from feeding_status table if the input is not null and different from the existing data
    IF p_feed_status_name IS NOT NULL THEN
        SET feed_status_id_val = (
		SELECT status_id 
		FROM feeding_status 
		WHERE status_name = p_feed_status_name
		LIMIT 1
	);
    END IF;

    -- Update feeding_log table
    UPDATE feeding_log
    SET fed_amount = IF(p_fed_amount IS NOT NULL, p_fed_amount, fed_amount),
        feed_status_id = IF(p_feed_status_name IS NOT NULL, feed_status_id_val, feed_status_id),
        food_tank_percent_before = IF(p_food_tank_percent_before IS NOT NULL, p_food_tank_percent_before, food_tank_percent_before),
        food_tank_percent_after = IF(p_food_tank_percent_after IS NOT NULL, p_food_tank_percent_after, food_tank_percent_after),
        process_started_when = IF(p_process_started_when IS NOT NULL, p_process_started_when, process_started_when),
        process_finished_when = IF(p_process_finished_when IS NOT NULL, p_process_finished_when, process_finished_when),
        updated_when = CURRENT_TIMESTAMP()
    WHERE feeding_log_id = p_log_id;

END;

GRANT EXECUTE ON PROCEDURE update_feeding_log_v8 TO 'app_rw'@'%';