CREATE PROCEDURE insert_feeder_transaction_v9(
    IN p_feeding_log_id INT,
    IN p_feeder_id TINYINT,
    IN p_payment_type_name VARCHAR(128),
    IN p_user_identifier VARCHAR(256),
    IN p_money_value TINYINT
) BEGIN
DECLARE payment_type_id_val TINYINT;
DECLARE if_user_exist BOOL ;
DECLARE user_id_val TINYINT;
DECLARE money_in_machine INT;
-- Set session transaction isolation level
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET payment_type_id_val = (
        SELECT payment_type_id
        FROM payment_type
        WHERE payment_type_name = p_payment_type_name
        LIMIT 1
    );
SET if_user_exist = (
        SELECT EXISTS(
                SELECT *
                FROM user_table
                WHERE user_identifier = p_user_identifier
                limit 1
            )
    );
IF if_user_exist = false THEN
    INSERT INTO user_table (
            user_name,
            user_type_id,
            user_identifier,
            recorded_when
        )
    VALUES('N\A', 1, p_user_identifier, NOW());
END IF;
SET user_id_val = (
        SELECT user_id
        FROM user_table
        WHERE user_identifier = p_user_identifier
        order by recorded_when
        LIMIT 1
    );
-- Insert into feeder_transaction table
INSERT INTO feeder_transaction (
        feeding_log_id,
        feeder_id,
        payment_type_id,
        user_id,
        money_value,
        recorded_when
    )
VALUES (
        p_feeding_log_id,
        p_feeder_id,
        payment_type_id_val,
        user_id_val,
        p_money_value,
        CURRENT_TIMESTAMP()
    );
SET money_in_machine = (
        SELECT total_money + p_money_value
        FROM feeder_money
        WHERE feeder_id = p_feeder_id
        ORDER BY recorded_when DESC
        LIMIT 1
    );
INSERT INTO feeder_money (feeder_id, total_money, recorded_when)
VALUES (p_feeder_id, money_in_machine, CURRENT_TIMESTAMP());
END;
GRANT EXECUTE ON PROCEDURE insert_feeder_transaction_v9 TO 'app_rw' @'%';