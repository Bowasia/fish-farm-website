CREATE PROCEDURE select_food_log_v1(
    IN p_date_start DATE,
    IN p_date_end DATE
)
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SELECT 
        recorded_when AS recorded_when
        , food_tank_percent_before AS food_percent
    FROM feeding_log
    WHERE recorded_when BETWEEN p_date_start AND p_date_end
    ORDER BY recorded_when;
END

GRANT EXECUTE ON PROCEDURE select_food_log_v1 TO 'app_rw'@'%';