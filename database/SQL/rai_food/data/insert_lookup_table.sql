insert into feeder VALUES (1, '1st prototype', true, NOW(), NULL);

insert into feeding_status VALUES (1, 'done', NOW(), NULL);
insert into feeding_status VALUES (2, 'in progress', NOW(), NULL);
insert into feeding_status VALUES (3, 'failed', NOW(), NULL);
insert into feeding_status VALUES (4, 'empty food bucket', NOW(), NULL);
insert into feeding_status values (5, 'in queue', NOW(), NULL);

insert into operation_type VALUES (1, 'scheduled', NOW(), NULL);
insert into operation_type VALUES (2, 'pay', NOW(), NULL);
insert into operation_type VALUES (3, 'manual', NOW(), NULL);

insert into payment_type  VALUES (1, 'coin', NOW(), NULL);
insert into payment_type  VALUES (2, 'line pay', NOW(), NULL);

insert into user_type VALUES (1, 'line user', NOW(), NULL);
insert into user_type VALUES (2, 'admin', NOW(), NULL);
insert into user_type VALUES (3, 'customer', NOW(), NULL);

insert into user_table VALUES (1, 'charnkanit kaewwong', 2, '63011394', NOW(), NULL);
insert into user_table VALUES (2, 'machine', 3, 'machine', NOW(), NULL);