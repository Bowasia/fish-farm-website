CREATE PROCEDURE get_image_by_id_v5(
    IN image_id          INT
    )
BEGIN
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SELECT filename, data, recorded_when
    FROM images  
    where id = image_id;       
END


GRANT EXECUTE ON PROCEDURE rai_detection.get_image_by_id_v5 TO 'app_rw'@'%';