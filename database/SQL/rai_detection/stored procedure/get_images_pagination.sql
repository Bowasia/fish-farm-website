CREATE PROCEDURE get_images_pagination_v5(
    IN p_pagination    INT,
    IN p_limit            INT,
    IN date_start       DATETIME,
    IN date_end         DATETIME
    )
BEGIN
    DECLARE p_offset INT DEFAULT 0;
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    SET p_offset = (p_pagination - 1) * p_limit;

    SELECT id, filename, data, recorded_when
    FROM images  
    WHERE recorded_when BETWEEN date_start and date_end
    ORDER BY recorded_when DESC
    LIMIT p_offset, p_limit;   
END


GRANT EXECUTE ON PROCEDURE get_images_pagination_v5 TO 'app_rw'@'%';
