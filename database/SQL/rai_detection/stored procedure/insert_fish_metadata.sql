CREATE PROCEDURE insert_fish_metadata_v4(
    IN image_id INT,
    IN fish_metadata JSON
) BEGIN
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DROP TEMPORARY TABLE IF EXISTS temp_fish_metadata;
CREATE TEMPORARY TABLE temp_fish_metadata (
    fish_breed VARCHAR(64),
    fish_count MEDIUMINT,
    fish_density_per_area MEDIUMINT,
    estimate_total_fish MEDIUMINT
);
INSERT INTO temp_fish_metadata
SELECT *
FROM JSON_TABLE(
        fish_metadata,
        '$[*]' COLUMNS (
            fish_breed VARCHAR(64) PATH '$.breed',
            fish_count MEDIUMINT PATH '$.count',
            fish_density_per_area   MEDIUMINT PATH '$.fish_density',
            estimate_total_fish MEDIUMINT PATH '$.estimate'
        )
    ) AS temp;

INSERT INTO image_fish_mapping (
        image_id,
        breed_id,
        fish_amount,
        fish_density_per_area,
        estimate_total_fish,
        recorded_when
    )
SELECT image_id,
    fb.breed_id,
    tfm.fish_count,
    tfm.fish_density_per_area,
    tfm.estimate_total_fish,
    CURRENT_TIMESTAMP()
FROM temp_fish_metadata as tfm
INNER JOIN fish_breed as fb
ON fb.breed_name = tfm.fish_breed;

DROP TEMPORARY TABLE IF EXISTS temp_fish_metadata;
END


GRANT EXECUTE ON PROCEDURE insert_fish_metadata_v4 TO 'app_rw' @'%';
