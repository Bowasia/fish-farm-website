CREATE TABLE images (
    id              INT AUTO_INCREMENT PRIMARY KEY,
    filename        VARCHAR(255)    NOT NULL,
    data            MEDIUMBLOB      NOT NULL,
    recorded_when   DATETIME        NOT NULL
);

