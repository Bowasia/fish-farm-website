GRANT SELECT ON qa_rai_sensor.* TO 'app_rw'@'%';
GRANT INSERT ON qa_rai_sensor.* TO 'app_rw'@'%';
GRANT DELETE ON qa_rai_sensor.* TO 'app_rw'@'%';
GRANT UPDATE ON qa_rai_sensor.* TO 'app_rw'@'%';

GRANT SELECT ON qa_rai_food.* TO 'app_rw'@'%';
GRANT INSERT ON qa_rai_food.* TO 'app_rw'@'%';
GRANT DELETE ON qa_rai_food.* TO 'app_rw'@'%';
GRANT UPDATE ON qa_rai_food.* TO 'app_rw'@'%';

GRANT SELECT ON qa_rai_detection.* TO 'app_rw'@'%';
GRANT INSERT ON qa_rai_detection.* TO 'app_rw'@'%';
GRANT DELETE ON qa_rai_detection.* TO 'app_rw'@'%';
GRANT UPDATE ON qa_rai_detection.* TO 'app_rw'@'%';

FLUSH PRIVILEGES;

GRANT ALL PRIVILEGES ON *.* TO 'admin_user'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
