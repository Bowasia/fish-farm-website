#!/usr/bin/env bash

# ensure to stop running container
docker  stop    rai-fish-farm-be-qa
docker  stop    rai-fish-farm-fe-qa

docker  rm      rai-fish-farm-be-qa
docker  rm      rai-fish-farm-fe-qa

# delete all images that's not tagged and not being used by any container
docker images prune

# pull latest db
echo "$DOCKER_PASSWORD" | docker login -u charnkanit --password-stdin https://index.docker.io/v1/
# backend
docker  pull    charnkanit/rai-fish-farm-be-qa:latest
docker  run --add-host=host.docker.internal:172.17.0.1 -d --name rai-fish-farm-be-qa -p 4000:4000 charnkanit/rai-fish-farm-be-qa:latest 
# frontend
docker  pull    charnkanit/rai-fish-farm-fe-qa:latest
docker  run -d --name rai-fish-farm-fe-qa -p 3002:3002 charnkanit/rai-fish-farm-fe-qa:latest 
