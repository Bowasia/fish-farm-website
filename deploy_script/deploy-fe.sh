#!/usr/bin/env bash

# ensure to stop running container
docker  stop    rai-fish-farm-fe-qa

docker  rm      rai-fish-farm-fe-qa

# delete all images that's not tagged and not being used by any container
docker images prune

# pull latest db
# frontend
docker  pull    charnkanit/rai-fish-farm-fe-qa:latest
docker  run -d --name rai-fish-farm-fe-qa -p 3002:3002 charnkanit/rai-fish-farm-fe-qa:latest