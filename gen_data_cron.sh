#!/bin/bash

# Function to generate temperature
generate_temperature() {
    HOUR=$(date +"%H")
    MINUTE=$(date +"%M")

    if [ $HOUR -lt 15 ]; then
        # Sigmoid function for gradual increase from 8 AM to 3 PM
        TIME_DIFF=$(( ($HOUR * 60 + $MINUTE - 8 * 60) ))
        TEMPERATURE=$(( 10 + (33 - 11) * $(echo "1 / (1 + e(-0.015 * $TIME_DIFF))" | bc -l) ))
    else
        # Sigmoid function for gradual decrease from 3 PM to 6 PM
        TIME_DIFF=$(( ($HOUR * 60 + $MINUTE - 15 * 60) ))
        TEMPERATURE=$(( 33 + ( 29 -33 ) * $(echo "1 / (1 + e(-0.01 * $TIME_DIFF))" | bc -l) ))
    fi
    echo $TEMPERATURE
}

# Function to generate JSON data
generate_json() {
    # Generate current date and time
    CURRENT_DATE=$(date +"%Y-%m-%d %H:%M:%S")

    # Generate temperature
    TEMPERATURE=$(generate_temperature)

    # Generate random values for other fields
    DISSOLVED_OXYGEN=$(LC_NUMERIC="en_US.UTF-8" printf "%.2f\n" $(awk -v min=6.5 -v max=7.0 'BEGIN{srand(); print min+rand()*(max-min)}'))
    PH=$(LC_NUMERIC="en_US.UTF-8" printf "%.2f\n" $(awk -v min=10 -v max=11 'BEGIN{srand(); print min+rand()*(max-min)}'))
    ELECTRIC_CONDUCTIVITY=$(LC_NUMERIC="en_US.UTF-8" printf "%.2f\n" $(awk -v min=0 -v max=1 'BEGIN{srand(); print min+rand()*(max-min)}'))
    SENSOR_NODE_ID=1

    # Construct JSON object
    JSON_OBJ="{\"sensor_node_id\": $SENSOR_NODE_ID, \"dissolved_oxygen_ppm\": $DISSOLVED_OXYGEN, \"electric_conductivity_ms_cm\": $ELECTRIC_CONDUCTIVITY, \"ph\": $PH, \"temperature_c\": $TEMPERATURE, \"recorded_when\": \"$CURRENT_DATE\"}"

    # Output JSON object
    echo $JSON_OBJ
    curl -X POST -H "Content-Type: application/json" -d "$JSON_OBJ1" https://smartfish.raikmitl.com/api/insertSensorData
}

# Generate JSON data and output
generate_json
