// codeUtil.js
function simplifyTime(originalTime) {
  const date = new Date(originalTime);

  const options = {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  };

  return date.toLocaleString("en-US", options);
}

function getDate(day_num) {
  const regex = /T.*?Z/g;
  const date = new Date();
  date.setHours(0);
  date.setMinutes(0);
  date.setSeconds(1);
  date.setDate(date.getDate() - day_num);
  return date;
}
function formatDateRange(date_start, date_end, day_num) {
  const regex = /T.*?Z/g;

  if (!date_start) date_start = getDate(day_num);
  else date_start = date_start.replace(regex, " 00:00:01.000");

  if (!date_end) {
    date_end = new Date();
    date_end.setHours(23);
    date_end.setMinutes(59);
    date_end.setSeconds(59);
  } else date_end = date_end.replace(regex, " 23:59:59.000");
  console.log(date_start, date_end);
  return { date_start_z: date_start, date_end_z: date_end };
}
module.exports = {
  simplifyTime,
  getDate,
  formatDateRange,
};
