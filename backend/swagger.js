const swaggerAutogen = require('swagger-autogen')()

const outputFile = './swagger_output.json'
const endpointsFiles = ['./endpoints.js']

const doc = {
    info: {
      title: 'fish farm api',
      description: 'Description',
    },
    host: 'localhost:4000',
    schemes: ['http'],
  };

swaggerAutogen(outputFile, endpointsFiles, doc)